## [Unreleased]

## [0.1.2] - 2021-12-28

- Implement methods: predecessor successor and gt_node for tree;
- Implement egt_key  instead of detect method;

## [0.1.1] - 2021-12-28

- Implement `RangeList::Tree` and `RangeList::Node`;
- Improve `RangeList` and add TODO comments for future work;

## [0.1.0] - 2021-12-26

- Initial project;
- Defined `RangeList` class to collect ranges;
- Add `RangeList#add` method;
- Add `RangeList#remove` method;
- Add `RangeList#to_a` method;
- Add `RangeList#print` method.
