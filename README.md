# RangeList

This is an exam repo, it is not a production ready library, just for a code exam from an job interview. please feel free to check it out if you like. cheers!!! 

## Installation


Clone the repo via the command below:

```shell
$ git clone git@gitlab.com:Mrjiolee/range_list.git
```

And then execute the following command to install dependencies:

```
$ cd range_list && bin/setup
```

## Run tests

After executing `bin/setup`, run `rake test` to run the tests.

## Usage

Add range to a range list with the required option `[range_begin, range_end]`:

```ruby
rl = RangeList.new
range_begin, range_end = 1, 5
rl.add([range_begin, range_end]) 
rl.to_a # You'll get [1...5]

range_begin, range_end = 4, 10
rl.add([range_begin, range_end])
rl.to_a # You'll get [1...10], two overlapped ranges were merged into a combined range
```

Remove all associated ranges from a range list with the required option `[range_begin, range_end]`:

```ruby
rl = RangeList.new
rl.add([1, 5])
rl.add([10, 20])
rl.add([25, 30])
range_begin, range_end = 1, 4
rl.remove([range_begin, range_end])
rl.to_a # You'll get [4...5, 10...20, 25...30]
range_begin, range_end = 1, 21
rl.remove([range_begin, range_end])
rl.to_a # You'll get [25...30] only, other ranges were removed from the list
```

## License

The gem is available as open source under the terms of the [MIT License](https://opensource.org/licenses/MIT).
