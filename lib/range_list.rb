# frozen_string_literal: true

require_relative "range_list/version"
require_relative "range_list/tree"
require_relative "range_list/node"

class RangeList
  attr_accessor :ranges, :tree

  def initialize
    @tree = Tree.new
    @ranges = {}
  end

  def sorted_keys
    @tree.sorted_keys
  end

  def to_a
    sorted_keys.inject([]) do |arr, key|
      arr << ranges[key]
    end
  end

  def add(range)
    r = Range.new(*range, true)

    return ranges if r.size == 0

    if ranges.empty?
      @ranges[r.end] = r
      @tree.root = Node.new(r.end)
      return ranges
    end

    new_range = merge(r)
    cleanup_outdated_range(r)

    @tree.insert(Node.new(new_range.end)) if @ranges[new_range.end].nil?
    @ranges[new_range.end] = new_range
    ranges
  end

  def remove(range)
    r = Range.new(*range, true)

    return ranges if r.size == 0
    return ranges if ranges.empty?

    new_begin_range, new_end_range = split(r)
    cleanup_outdated_range(r)

    if new_begin_range
      @tree.insert(Node.new(new_begin_range.end)) if @ranges[new_begin_range.end].nil?
      @ranges[new_begin_range.end] = new_begin_range
    end

    if new_end_range
      @tree.insert(Node.new(new_end_range.end)) if @ranges[new_end_range.end].nil?
      @ranges[new_end_range.end] = new_end_range
    end

    ranges
  end

  def print
    puts(to_a.inject(''){|output, range|
      "#{output} [#{range.begin}, #{range.end})"
    }.strip)
  end

  def inspect
    to_a.inspect
  end

  def egt_key(key)
    return key if !ranges[key].nil?

    node = @tree.gt_node(key)
    node.nil? ? nil : node.key
  end

  private

  def merge(range)
    new_begin, new_end = range.begin, range.end
    begin_range_key = egt_key(range.begin)
    end_range_key = egt_key(range.end)

    if !begin_range_key.nil?
      begin_range = ranges[begin_range_key]
      new_begin = begin_range.begin if range.begin >= begin_range.begin
    end

    if !end_range_key.nil?
      end_range = ranges[end_range_key]
      new_end = end_range.end if range.end >= end_range.begin
    end
    Range.new(new_begin, new_end, true)
  end

  def split(range)
    new_begin_range, new_end_range = nil, nil

    begin_range_key = egt_key(range.begin)
    end_range_key = egt_key(range.end)

    if !begin_range_key.nil?
      begin_range = ranges[begin_range_key]
      if range.begin >= begin_range.begin
        begin_range_begin = begin_range.begin
        begin_range_end = range.begin
        new_begin_range = Range.new(begin_range_begin, begin_range_end, true) if begin_range_begin < begin_range_end
      end
    end

    if !end_range_key.nil?
      end_range = ranges[end_range_key]
      if range.end > end_range.begin
        end_range_end = end_range.end
        end_range_begin = range.end
        new_end_range = Range.new(end_range_begin, end_range_end, true) if end_range_begin < end_range_end
      end
    end

    return new_begin_range, new_end_range
  end

  def cleanup_outdated_range(range)
    keys = sorted_keys
    expired_range_keys = keys.select{ |k| range.include?(k) || k == range.end } # TODO: improve the code
    expired_range_keys.each do |k|
      ranges.delete(k)
      node = @tree.find(k)
      @tree.delete(node)
    end
  end
end

