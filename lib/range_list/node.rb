class RangeList
  class Node # :nodoc
    attr_accessor :left, :right, :parent, :key

    def initialize(key)
      @left = left
      @right = right
      @parent = parent
      @key = key
    end

    def minimum_node
      node = self
      while !node.left.nil?
        node = node.left
      end
      node
    end

    def maximum_node
      node = self
      while !node.right.nil?
        node = node.right
      end
      node
    end
  end
end
