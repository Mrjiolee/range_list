require "range_list/node"

class RangeList
  class Tree # :nodoc
    attr_accessor :root

    def initialize
      @root = nil
    end

    def minimum_node
      @root.minimum_node
    end

    def maximum_node
      @root.maximum_node
    end

    def sorted_keys
      keys = []
      walk(root){ |node| keys << node.key }
      keys
    end

    def walk(node, &block)
      if !node.nil?
        walk(node.left, &block)
        if block_given?
          block.call(node)
        end
        walk(node.right, &block)
      end
    end

    def search(node, key)
      return node if node.nil? or key == node.key
      key < node.key ? search(node.left, key) : search(node.right, key)
    end

    def find(key)
      search(@root, key)
    end

    def gt_node(key)
      backup_node = nil
      begin_node = @root

      while !begin_node.nil?
        backup_node = begin_node
        if key < begin_node.key
          begin_node = begin_node.left
        else
          begin_node = begin_node.right
        end
      end

      return backup_node if backup_node.nil?
      return backup_node if key < backup_node.key
      successor(backup_node)
    end

    def insert(node)
      backup_node = nil
      begin_node = @root

      while !begin_node.nil?
        backup_node = begin_node
        if node.key < begin_node.key
          begin_node = begin_node.left
        else
          begin_node = begin_node.right
        end
      end

      node.parent = backup_node

      if backup_node.nil?
        @root = node
      elsif node.key < backup_node.key
        backup_node.left = node
      else
        backup_node.right = node
      end
    end

    def delete(node)
      if node.left.nil?
        transplant(node, node.right)
      elsif node.right.nil?
        transplant(node, node.left)
      else
        min_node = node.right.minimum_node

        if min_node.parent != node
          transplant(min_node, min_node.right)
          min_node.right = node.right
          min_node.right.parent = min_node
        end

        transplant(node, min_node)

        min_node.left = node.left
        min_node.left.parent = min_node
      end
    end

    def successor(node)
      if !node.right.nil?
        return node.right.minimum_node
      end
      backup_node = node.parent
      while !backup_node.nil? && node == backup_node.right
        node = backup_node
        backup_node = backup_node.parent
      end
      backup_node
    end

    def predecessor(node)
      if !node.left.nil?
        return node.left.maximum_node
      end

      backup_node = node.parent
      while !backup_node.nil? && node == backup_node.left
        node = backup_node
        backup_node = backup_node.parent
      end
      backup_node
    end

    private

    def transplant(old_node, new_node)
      if old_node.parent.nil?
        @root = new_node
      elsif old_node == old_node.parent.left
        old_node.parent.left = new_node
      else
        old_node.parent.right = new_node
      end

      if !new_node.nil?
        new_node.parent = old_node.parent
      end
    end
  end
end
