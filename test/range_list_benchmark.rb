require "test_helper"
require "minitest/benchmark"

class RangeListBenchmark < Minitest::Benchmark
  def setup
    @range_list = RangeList.new
  end

  def bench_range_list_add
    assert_performance_linear do |n|
      n.times do |i|
        @range_list.add([i + i, i + i + 1])
      end
    end
  end
end
