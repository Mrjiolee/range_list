# frozen_string_literal: true

require "test_helper"

class TestRangeList < Minitest::Test
  def setup
    @range_list = RangeList.new
  end

  def test_add_range_to_list
    # [1, 5]
    range_begin = 1
    range_end = 5
    @range_list.add([range_begin, range_end])
    assert_equal [1...5], @range_list.to_a

    # [10, 20]
    range_begin, range_end = 10, 20
    @range_list.add([range_begin, range_end])
    assert_equal [1...5, 10...20], @range_list.to_a

    # [20, 20]
    range_begin, range_end = 20, 20
    @range_list.add([range_begin, range_end])
    assert_equal [1...5, 10...20], @range_list.to_a

    # [20, 21]
    range_begin, range_end = 20, 21
    @range_list.add([range_begin, range_end])
    assert_equal [1...5, 10...21], @range_list.to_a

    # [2, 4]
    range_begin, range_end = 2, 4
    @range_list.add([range_begin, range_end])
    assert_equal [1...5, 10...21], @range_list.to_a

    # [3, 8]
    range_begin, range_end = 3, 8
    @range_list.add([range_begin, range_end])
    assert_equal [1...8, 10...21], @range_list.to_a
  end

  def test_remove_range_from_list
    @range_list.add([1, 8])
    @range_list.add([10, 21])

    # [10, 10]
    range_begin, range_end = 10, 10
    @range_list.remove([range_begin, range_end])
    assert_equal [1...8, 10...21], @range_list.to_a

    # [10, 11]
    range_begin, range_end = 10, 11
    @range_list.remove([range_begin, range_end])
    assert_equal [1...8, 11...21], @range_list.to_a

    # [15, 17]
    range_begin, range_end = 15, 17
    @range_list.remove([range_begin, range_end])
    assert_equal [1...8, 11...15, 17...21], @range_list.to_a

    # [3, 19]
    range_begin, range_end = 3, 19
    @range_list.remove([range_begin, range_end])
    assert_equal [1...3, 19...21], @range_list.to_a
  end

  def test_range_list_to_a_return_sorted_range_array
    add_opts = [[2, 4], [10, 20], [7, 8], [5, 6]]
    add_opts.each{ |o| @range_list.add(o) }
    result = [2...4, 5...6, 7...8, 10...20]
    assert_equal result, @range_list.to_a
  end
end
